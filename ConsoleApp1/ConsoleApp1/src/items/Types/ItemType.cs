﻿using ConsoleApp1.src.items.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.src.items
{
    //Всё то, что содержат предметы
    public class ItemType
    {
        public void SetSubType(SubType subType)
        {
            this.subType = subType;
        }
        public void SetPoints(short points)
        {
            this.points = points;
        }
        public void SetName(string name)
        {
            this.name = name;
        }
        public void SetLore(string Lore)
        {
            this.Lore = Lore;
        }
        public void SetIcon(char icon)
        {
            this.icon = icon;
        }

        public char GetIcon()
        {
            return icon;
        }

        public SubType GetSubType()
        {
            return subType;
        }
        public short GetPoints()
        {
            return points;
        }
        public string GetName()
        {
            return name;
        }
        public string GetLore()
        {
            return Lore;
        }
        //Методы
        public void UseItem()
        {
            subType = SubType.NONE;
        }
        //Определение, к чему относится данный предмет
        private SubType subType;
        //Кол-во профита от предмета
        private short points;
        //Название предмета
        private string name;
        //Описание
        private string Lore;
        //Как выглядит
        private char icon;
    }
}
