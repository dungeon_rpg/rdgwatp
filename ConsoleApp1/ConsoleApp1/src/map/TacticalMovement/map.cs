﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Globalization;
using ConsoleApp1.src.creatures.builders;
using ConsoleApp1.src.creatures.director;
using ConsoleApp1.src.items;
using ConsoleApp1.src.map.TacticalMovement.LocalVars;
using ConsoleApp1.src.map.TacticalMovement.Strategies;

namespace ConsoleApp1.src.map
{

    class map
    {
        //Обновление фрейма с каждым шагом 
        private static void RefreshFrame(ref List<CreatureBuilder> Lcb)
        {
            foreach (CreatureBuilder cb in Lcb)
            {
                //Обновление местонахождения каждого из существ
                Rendering.WriteAt(CreatedMap.testMap[cb.getLastX(),cb.getLastY()], cb.getLastX(), cb.getLastY());
                Rendering.WriteAt(cb.getIcon(), cb.getX(), cb.getY());
            }
        }
        public static void StartMap()
        {
            const byte PcoorX = 1, //Тестовые координаты для игрока
                       PcoorY = 1;

           // CreatedMap.fillMap();
            CreatedMap.CreateGraph(); // Создание графа для карты, по нему перемещаются существа
            //Создание оболочек для существ
            CreatureBuilder cbP = new CreatureBuilder();
            CreatureBuilder cbS = new CreatureBuilder();
            CreatureBuilder cbL = new CreatureBuilder();
            CreatureBuilder cbK = new CreatureBuilder();
            CreatureBuilder cbG = new CreatureBuilder();
            Director director = new Director();

            //Заполнение оболочек для существ
            director.constructPlayer(cbP, PcoorX, PcoorY);
            //director.constructSkeleton(cbS, PcoorX+5, PcoorY+5);
            //director.constructSlime(cbL, PcoorX+5, PcoorY+5);
            director.constructDoorKnob(cbK, PcoorX+5, PcoorY+5);
            director.constructGhost(cbG, PcoorX + 5, PcoorY + 4);

            //Список существ
            List<CreatureBuilder> Lcb = new List<CreatureBuilder>();
            Lcb.Add(cbP);
            //Lcb.Add(cbS);
            //Lcb.Add(cbL);
            Lcb.Add(cbK);
            Lcb.Add(cbG);
            CreatedMap.ShowMap();
            //Задание кодировки (надо ли сейчас, хз)
            Console.OutputEncoding = Encoding.Unicode;
            RefreshFrame(ref Lcb);
            //CreatedMap.testMap[cbP.getX(), cbP.getY()] = cbP.getIcon();
            //CreatedMap.testMap[cbS.getX(), cbS.getY()] = cbS.getIcon();
            //CreatedMap.testMap[cbL.getX(), cbL.getY()] = cbL.getIcon();
            //CreatedMap.testMap[cbK.getX(), cbK.getY()] = cbK.getIcon();
            ConsoleKeyInfo keyInfo;

            //Тут с каждым шагом выполняется та или иная стратегия (они не особо отличаются)
            var context = new Context();
            do
            {
                keyInfo = Console.ReadKey();
                switch (keyInfo.Key)
                {
                    case ConsoleKey.UpArrow:
                        context.SetStrategy(new StrategyGoUp());//Шаг вверх
                        break;
                    case ConsoleKey.DownArrow:
                        context.SetStrategy(new StrategyGoDown());//Шаг вниз
                        break;
                    case ConsoleKey.LeftArrow:
                        context.SetStrategy(new StrategyGoLeft());//Шаг Влево
                        break;
                    case ConsoleKey.RightArrow:
                        context.SetStrategy(new StrategyGoRight());//Шаг вправо
                        break;
                    case ConsoleKey.I:
                        context.SetStrategy(new StrategyOpenInventory());//Открыть инвентарь
                        break;
                    default:
                        continue;
                }
                //Выполнение той или иной стратегии
                context.DoSomeBusinessLogic(ref Lcb);
                //Удаление всех существ с хп ниже 1
                Lcb.RemoveAll(c => c.getHP() < 1);
                //Обновление фрейма
                RefreshFrame(ref Lcb);
                //Game over?
            } while (keyInfo.Key != ConsoleKey.Escape);

        }
    }
}
