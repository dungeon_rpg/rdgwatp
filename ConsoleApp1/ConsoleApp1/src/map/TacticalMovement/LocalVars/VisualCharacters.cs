﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.src.map.TacticalMovement.LocalVars
{
    //VisualCharacter - в общем случае вседоступные визуальные оформления
    public static class VisualCharacters
    {
        
        public static char[] WallPattern = new char[] {'*'};
        public static char[] FloorPattern = new char[] { (char)9632 }; //Изменится с рандомизацией мапы
        public static char[,] EmptyInventoryCase = new char[,] { {' ', '෴', ' ' }, 
                                                                 { '┫', ' ', '┣' }, 
                                                                 { ' ', 'ᚚ', ' ' }, };

        public static char ChosenElementPattern = (char)9632;
        public static char ChosenElementPatternNONchosen = ' ';
        public static byte caseSize = (byte)EmptyInventoryCase.GetLongLength(0);
    }
}
